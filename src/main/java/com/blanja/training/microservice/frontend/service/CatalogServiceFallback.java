package com.blanja.training.microservice.frontend.service;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.blanja.training.microservice.frontend.dto.Product;

@Component
public class CatalogServiceFallback implements CatalogService{

	@Override
    public Iterable<Product> allProducts() {
        return new ArrayList<>();
    }

}
