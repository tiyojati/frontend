package com.blanja.training.microservice.frontend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import com.blanja.training.microservice.frontend.service.CatalogService;

@Controller
public class ProductController {

	@Autowired CatalogService catalogService;
	
	@GetMapping("/product/list")
    public ModelMap daftarProduct() {
        return new ModelMap()
                .addAttribute("productData", catalogService.allProducts());
    }

}
