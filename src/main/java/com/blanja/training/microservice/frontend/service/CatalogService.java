package com.blanja.training.microservice.frontend.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.blanja.training.microservice.frontend.dto.Product;

@FeignClient(name = "catalog",fallback = CatalogServiceFallback.class)
public interface CatalogService {

	@GetMapping("/api/product/allProducts")
    public Iterable<Product> allProducts();

}
